<?php

namespace Alis\Http\Controllers;

use Illuminate\Http\Request;

use Alis\Http\Requests;
use Alis\Http\Controllers\Controller;

class PagesController extends Controller
{
    public function about(){

    	return view('static/about');
    }
}
