<!DOCTYPE html>
<html>
    <head>
        <title>JMIR Developer Wiki</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="{{ URL::asset('css/github-markdown.css') }}">
        <style>
            .markdown-body {
                box-sizing: border-box;
                min-width: 200px;
                max-width: 980px;
                margin: 0 auto;
                padding: 45px;
            }
        </style>
    </head>
    <body >
        <header></header>
        <main class="markdown-body">
Laravel MVC Overview
======
Notes on how to use MVC in Laravel

## Server Config
Environment variables are in .env

## Routes
/app/Http/routes.php

## Model
$ php artisan make:model {Model}

## View
/resources/views  

Unescaped var: <?php echo urldecode('%7B')?>!! $name !!<?php echo urldecode('%7D')?>
<br>
Escaped var: <?php echo urldecode('%7B')?><?php echo urldecode('%7B')?> $name <?php echo urldecode('%7D')?><?php echo urldecode('%7D')?>
<br>
Master pages: yield, extend, etc...

## Controller
$ php artisan make:controller PagesController

Pass var to view:
return view('generic/page')->with([
    'first' => 'Fname',
    'last' => 'Lname'
]);

return view('generic/page', $data_arr);

## Migration - version control for db!
$ php artisan help make:migration

        </main>
        <footer>
            <script type="text/javascript" src="{{ URL::asset('js/jquery-2.2.0.min.js') }}"></script>
            <script type="text/javascript" src="{{ URL::asset('js/showdown.min.js') }}"></script>
            <script type="text/javascript">
                $( document ).ready(function() {
                    var converter = new showdown.Converter(),
                    text = $('.markdown-body').html(),
                    html = converter.makeHtml(text);

                    $('.markdown-body').html(html);
                });
            </script>
        </footer>    
    </body>
</html>
